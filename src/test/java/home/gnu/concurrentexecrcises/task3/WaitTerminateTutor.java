package home.gnu.concurrentexecrcises.task3;

import org.junit.Test;

/**
 * Как остановить поток?
 * <p>
 * Для того, чтобы прервать поток, мы можем использовать флаг
 * shouldTerminate, который должен проверяться в цикле внутри run().
 * Если флаг становится true, мы просто выходим из цикла.
 * <p>
 * Однако, тут могут быть проблемы, если от нашего потока зависят другие потоки.
 * В настоящий момент поток t2 прерывается, и программа подвисает,
 * т.к. поток t1 ждет второй поток и не может дождаться.
 * Какие есть решения проблемы?
 * Есть как минимум два решения, с третим потоком и без него. Попробуйте найти оба.
 */
public class WaitTerminateTutor {
    private Thread t1, t2;
    private final Object monitor = new Object();
    private volatile int runningThreadNumber = 1;

    private class TestThread implements Runnable {
        private final String threadName;
        boolean shouldTerminate;

        TestThread(String threadName) {
            this.threadName = threadName;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                System.out.println(threadName + ":" + i);
                synchronized (monitor) {
                    waitToRun();
                    changeRunningThread();
                    sleep(100);
                    if (shouldTerminate) return;
                }
            }
        }

        private void waitToRun() {
            try {
                while (!threadName.equals("t" + runningThreadNumber)) {
                    System.out.println("wait for thread " + "t" + runningThreadNumber);
                    monitor.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void sleep(int ms) {
            try {
                Thread.sleep(ms);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void changeRunningThread() {
            runningThreadNumber++;
            if (runningThreadNumber > 2) runningThreadNumber = 1;
            monitor.notifyAll();
        }
    }

    @Test
    public void testThread() {
        TestThread testThread1 = new TestThread("t1");
        t1 = new Thread(testThread1);
        final TestThread testThread2 = new TestThread("t2");
        t2 = new Thread(testThread2);

        System.out.println("Starting threads...");
        t1.start();
        t2.start();

        Thread terminator = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            testThread2.shouldTerminate = true;
        });
        terminator.start();

        System.out.println("Waiting threads to join...");
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}