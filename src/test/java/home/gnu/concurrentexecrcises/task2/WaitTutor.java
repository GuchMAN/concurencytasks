package home.gnu.concurrentexecrcises.task2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Как сделать так, чтобы потоки вызывались по очереди?
 * <p>
 * Часто необходимо упорядочить потоки, т.к. результат одного потока
 * понадобится другому, и нужно дождаться, когда первый поток сделает свою работу.
 * <p>
 * Задача: добавьте еще один поток, который будет выводить в лог сообщения о
 * значениях счетчика, кратных 10, например 10, 20, 30...
 * При этом такие сообщения должны выводиться после того, как все потоки преодолели
 * кратность 10, но до того, как какой-либо поток двинулся дальше.
 */
public class WaitTutor {
    private static final int threadsCount = 2; // program valid only for 2 threads.
    protected static final int COUNT_TO = 99;

    private List<Thread> threads = new ArrayList<>(threadsCount);
    private int[] counters = new int[threadsCount];
    private CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

    private class TestThread implements Runnable {
        private final String threadName;
        private final int n;

        TestThread(int threadNumber) {
            this.threadName = "Thread#" + threadNumber;
            this.n = threadNumber - 1;
        }

        @Override
        public void run() {
            for (int i = 0; i <= COUNT_TO; i++) {
                System.out.println(threadName + ":" + i);
                updateCounter(i);
                try {
                    cyclicBarrier.await();
                    cyclicBarrier.await(); // wait for logger print
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
                Thread.yield();
            }
        }

        private void updateCounter(int i) {
            counters[n] = i;
            Thread.yield();
        }
    }

    @Test
    public void testThread() {
        for (int i = 1; i <= threadsCount; ++i) {
            threads.add(new Thread(new TestThread(i)));
        }
        Thread logger = new Thread(() -> {
            try {
                int currentCounter;
                do {
                    cyclicBarrier.await();
                    currentCounter = counters[0];
                    if (currentCounter != 0 && currentCounter % 10 == 0) {
                        System.out.println("Counters " + Arrays.toString(counters));
                    }
                    cyclicBarrier.await();
                } while (currentCounter != COUNT_TO);
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        });
        System.out.println("Starting threads");
        logger.start();
        threads.forEach(Thread::start);

        System.out.println("Waiting for threads");
        threads.forEach(this::silentlyJoin);
        silentlyJoin(logger);
    }

    private void silentlyJoin(Thread thread) {
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}