package home.gnu.concurrentexecrcises.task4;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;

@State(Scope.Benchmark)
@Fork(warmups = 1, value = 5, jvmArgs = {"-Xms1G", "-Xmx1G"})
@Measurement(iterations = 5)
@Warmup(iterations = 5)
@BenchmarkMode(Mode.Throughput)
public class Benchmarks {

    private static final int threadsCount = Runtime.getRuntime().availableProcessors();
    private static final int elementsCount = 10_000_000;
    private static final ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(threadsCount);
    private static final ForkJoinPool forkJoinPool = new ForkJoinPool(threadsCount);

    @Benchmark
    public AbstractSynchList atomicArray() {
        return process(new AtomicArray(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Benchmark
    public AbstractSynchList atomicIndex() {
        return process(new AtomicIndex(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Benchmark
    public AbstractSynchList synchronizedBlock() {
        return process(new SynchronizedBlock(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Benchmark
    public ParallelStream parallelStream() {
        ParallelStream parallelStreamImpl = new ParallelStream(elementsCount);
        parallelStreamImpl.fill();
        return parallelStreamImpl;
    }

    @Benchmark
    public AbstractSynchList diapasonFiller() {
        return process(new DiapasonFiller(threadsCount, elementsCount, threadPoolExecutor));
    }


    @Benchmark
    public ForkJoin forkJoin() {
        ForkJoin forkJoin = new ForkJoin(elementsCount, forkJoinPool);
        forkJoin.fill();
        return forkJoin;
    }

    //     @Benchmark // to slow to wait
    public AbstractSynchList copyOnWriteList() {
        return process(new CopyOnWriteList(threadsCount, elementsCount, threadPoolExecutor));
    }


    private AbstractSynchList process(AbstractSynchList impl) {
        impl.fill();
        return impl;
    }

    @TearDown
    public void tearDown() {
        threadPoolExecutor.shutdownNow();
    }

}
