package home.gnu.concurrentexecrcises.task4;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;

public abstract class AbstractSynchList {
    static final String[] LANGS = {"SQL", "PHP", "XML", "Java", "Scala", "Python", "JavaScript", "ActionScript", "Clojure", "Groovy", "Ruby", "C++"};

    private static final boolean PRINT = false; // for test management.
    private final int threadsCount;
    private final int elementsCount;
    private ExecutorService executor;

    public AbstractSynchList(int threadsCount, int elementsCount, ExecutorService executor) {
        this.threadsCount = threadsCount;
        this.elementsCount = elementsCount;
        this.executor = executor;
    }


    protected String getRandomLang() {
        return LANGS[ThreadLocalRandom.current().nextInt(LANGS.length)];
    }

    protected void print(Collection<?> collection) {
        if (!PRINT) {
            return;
        }
        StringBuilder builder = new StringBuilder();
        collection.forEach(lang -> builder.append(lang).append(" "));
        System.out.println(builder.toString());
    }

    public void fill() {
        List<Future> threads = range(0, threadsCount)
                .mapToObj(this::getFiller)
                .map(executor::submit)
                .collect(Collectors.toList());

        threads.forEach(this::futureGetSilently);
    }

    private void futureGetSilently(Future f) {
        try {
            f.get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new IllegalStateException(e);
        }
    }

    protected int getElementsCount() {
        return elementsCount;
    }

    protected int getThreadsCount() {
        return threadsCount;
    }

    protected int getElementsPerThreadCount() {
        return elementsCount / threadsCount;
    }

    protected abstract Runnable getFiller(int number);

    protected abstract String getResult(int i);
}
