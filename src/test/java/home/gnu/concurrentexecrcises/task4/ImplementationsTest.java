package home.gnu.concurrentexecrcises.task4;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

public class ImplementationsTest {
    private static final int threadsCount = Runtime.getRuntime().availableProcessors();
    private static final int elementsCount = 10_000;
    private static final ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(threadsCount);

    @Test
    public void atomicArray() {
          fillAndCheck(new AtomicArray(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Test
    public void atomicIndex() {
          fillAndCheck(new AtomicIndex(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Test
    public void copyOnWriteList() {
          fillAndCheck(new CopyOnWriteList(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Test
    public void synchronizedBlock() {
          fillAndCheck(new SynchronizedBlock(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Test
    public void diapasonFiller() {
          fillAndCheck(new DiapasonFiller(threadsCount, elementsCount, threadPoolExecutor));
    }

    @Test
    public void parallelStream() {
        ParallelStream parallelStreamImpl = new ParallelStream(elementsCount);
        parallelStreamImpl.fill();
        parallelStreamImpl.getResults().forEach(Assert::assertNotNull);
    }

    @Test
    public void forkJoin() {
        ForkJoin forkJoinImpl = new ForkJoin(elementsCount, new ForkJoinPool(4));
        forkJoinImpl.fill();
        forkJoinImpl.getResults().forEach(Assert::assertNotNull);
    }


    private void fillAndCheck(AbstractSynchList impl) {
        impl.fill();
        IntStream.range(0, elementsCount)
                .mapToObj(impl::getResult)
                .forEach(Assert::assertNotNull);
    }
}
