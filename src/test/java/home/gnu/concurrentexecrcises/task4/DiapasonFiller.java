package home.gnu.concurrentexecrcises.task4;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * Each thread fill it own diapason in list.
 */
public class DiapasonFiller extends AbstractSynchList {
    private final List<String> result;

    public DiapasonFiller(int chunksCount, int elementsCount, ExecutorService executor) {
        super(chunksCount * 2, elementsCount, executor);
        result = Arrays.asList(new String[elementsCount]);
    }

    private class LangFiller implements Runnable {
        private final String threadName;
        private final int from;
        private final int toExclusive;

        LangFiller(int threadNumber, int from, int toExclusive) {
            this.threadName = "Thread#" + threadNumber;
            this.from = from;
            this.toExclusive = toExclusive;
        }

        @Override
        public void run() {
            for (int i = from; i < toExclusive; ++i) {
                result.set(i, getRandomLang());
            }
        }
    }

    @Override
    protected Runnable getFiller(int number) {
        return new LangFiller(number, getElementsPerThreadCount() * number, getElementsPerThreadCount() * (number + 1));
    }

    @Override
    protected String getResult(int i) {
        return result.get(i);
    }
}
