package home.gnu.concurrentexecrcises.task4;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static home.gnu.concurrentexecrcises.task4.AbstractSynchList.LANGS;

/**
 * Fork join internals of parallel stream generate ranges of langs array and then merge.
 * But manual implementation will be faster
 */
public class ParallelStream {

    private List<String> randomLangs;
    private int elementsCount;

    public ParallelStream(int elementsCount) {
        this.elementsCount = elementsCount;
    }

    public List<String> getResults(){
        return randomLangs;
    }

    void fill() {
        randomLangs = IntStream.range(0, elementsCount)
                .parallel()
                .map(i -> ThreadLocalRandom.current().nextInt(LANGS.length))
                .mapToObj(i -> LANGS[i])
                .collect(Collectors.toList());
    }

}
