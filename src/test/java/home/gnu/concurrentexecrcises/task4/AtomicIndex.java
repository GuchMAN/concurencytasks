package home.gnu.concurrentexecrcises.task4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * AtomicInteger index based on CAS algorithm
 */
public class AtomicIndex extends AbstractSynchList {
    private final AtomicInteger atomicIndex = new AtomicInteger();
    private final String[] randomLangs = new String[getElementsCount()];

    public AtomicIndex(int threadsCount, int elementsCount, ExecutorService executor) {
        super(threadsCount, elementsCount, executor);
    }

    private class LangFiller implements Runnable {
        private final String threadName;

        LangFiller(int threadNumber) {
            this.threadName = "Thread#" + threadNumber;
        }

        @Override
        public void run() {
            for (int i = atomicIndex.getAndIncrement(); i < getElementsCount(); i = atomicIndex.getAndIncrement()) {
                randomLangs[i] = getRandomLang();
            }
        }
    }

    @Override
    protected Runnable getFiller(int number) {
        return new LangFiller(number);
    }

    @Override
    protected String getResult(int i) {
        return randomLangs[i];
    }
}
