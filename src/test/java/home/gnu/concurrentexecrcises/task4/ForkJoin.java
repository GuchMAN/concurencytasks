package home.gnu.concurrentexecrcises.task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.ThreadLocalRandom;

import static home.gnu.concurrentexecrcises.task4.AbstractSynchList.LANGS;

public class ForkJoin {

    protected static final int CHUNKS_COUNT = 20;
    private int threshold;
    private final ForkJoinPool forkJoinPool;
    private final int elementsCount;
    private final List<String> randomLangs;

    public ForkJoin(int elementsCount, ForkJoinPool forkJoinPool) {
        this.elementsCount = elementsCount;
        this.threshold = elementsCount / CHUNKS_COUNT;
        this.forkJoinPool = forkJoinPool;
        this.randomLangs = Arrays.asList(new String[elementsCount]);
    }

    public void fill() {
        List<ForkJoinTask> tasks = new ArrayList<>(elementsCount / threshold);
        for (int from = 0, to = threshold; from < elementsCount; from = to, to += threshold) {
            tasks.add(forkJoinPool.submit(new LangFiller(from, to)));
        }
        tasks.forEach(ForkJoinTask::join);
    }

    public List<String> getResults() {
        return randomLangs;
    }

    private class LangFiller implements Runnable {
        private final int from;
        private final int toExclusive;

        LangFiller(int from, int toExclusive) {
            this.from = from;
            this.toExclusive = toExclusive;
        }

        @Override
        public void run() {
            for (int i = from; i < toExclusive; ++i) {
                randomLangs.set(i, LANGS[ThreadLocalRandom.current().nextInt(LANGS.length)]);
            }
        }
    }
}
