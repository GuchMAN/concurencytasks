package home.gnu.concurrentexecrcises.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * Simply block on each add operations.
 */
public class SynchronizedBlock extends AbstractSynchList {
    private final List<String> randomLangs = new ArrayList<>(getElementsCount());

    public SynchronizedBlock(int threadsCount, int elementsCount, ExecutorService executor) {
        super(threadsCount, elementsCount, executor);
    }

    @Override
    protected Runnable getFiller(int number) {
        return new LangFiller(number);
    }

    @Override
    protected String getResult(int i) {
        return randomLangs.get(i);
    }

    private class LangFiller implements Runnable {
        private final String threadName;

        LangFiller(int threadNumber) {
            this.threadName = "Thread#" + threadNumber;
        }

        @Override
        public void run() {
            for (int i = 0; i < getElementsPerThreadCount(); i++) {
                String randomLang = getRandomLang();
                synchronized (randomLangs) {
                    randomLangs.add(randomLang);
                    print(randomLangs);
                }
            }
        }
    }
}
