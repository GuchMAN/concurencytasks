package home.gnu.concurrentexecrcises.task4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * Variant based on CAS operations on array.
 */
public class AtomicArray extends AbstractSynchList {
    private final AtomicReferenceArray<String> atomicIntegerArray = new AtomicReferenceArray<>(getElementsCount());

    public AtomicArray(int threadsCount, int elementsCount, ExecutorService executor) {
        super(threadsCount, elementsCount, executor);
    }

    private class LangFiller implements Runnable {
        private final String threadName;

        LangFiller(int threadNumber) {
            this.threadName = "Thread#" + threadNumber;
        }

        @Override
        public void run() {
            String nextLang = getRandomLang();
            for (int i = 0; i < getElementsCount(); ++i) {
                if (atomicIntegerArray.compareAndSet(i, null, nextLang)) {
                    nextLang = getRandomLang();
                }
            }
        }
    }

    @Override
    protected Runnable getFiller(int number) {
        return new LangFiller(number);
    }

    @Override
    protected String getResult(int i) {
        return atomicIntegerArray.get(i);
    }
}