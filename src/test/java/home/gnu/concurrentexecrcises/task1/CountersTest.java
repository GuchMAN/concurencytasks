package home.gnu.concurrentexecrcises.task1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountersTest {

    @Test
    public void testLongAdderBased() {
        int threadCount = 100;
        int incsPerThread = 1000;
        int result = new LongAdderBased(threadCount, incsPerThread).count();
        assertEquals(threadCount * incsPerThread, result);
    }

   @Test
    public void testIntegerBased() {
        int threadCount = 100;
        int incsPerThread = 1000;
        int result = new AtomicIntegerBased(threadCount, incsPerThread).count();
        assertEquals(threadCount * incsPerThread, result);
    }

}
