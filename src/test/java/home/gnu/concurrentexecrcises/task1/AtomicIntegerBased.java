package home.gnu.concurrentexecrcises.task1;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerBased extends AtomicCounterAbstract {
    private final AtomicInteger atomicInteger;

    public AtomicIntegerBased(int threadCount, int incsPerThread) {
        super(threadCount, incsPerThread);
        atomicInteger = new AtomicInteger();
    }

    @Override
    public int getResult() {
        return atomicInteger.intValue();
    }

    @Override
    public void inc() {
        atomicInteger.incrementAndGet();
    }
}
