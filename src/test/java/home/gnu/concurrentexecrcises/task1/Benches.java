package home.gnu.concurrentexecrcises.task1;

import org.openjdk.jmh.annotations.*;

@State(Scope.Benchmark)
@Fork(warmups = 1, value = 5, jvmArgs = {"-Xms1G", "-Xmx1G"})
@Measurement(iterations = 5)
@Warmup(iterations = 5)
@BenchmarkMode(Mode.Throughput)
public class Benches {
    private int threadCount = 100;
    private int incsPerThread = 1_000_000;

    @Benchmark
    public int longAdderBased() {
        return new LongAdderBased(threadCount, incsPerThread).count();
    }

    @Benchmark
    public int integerBased() {
        return new AtomicIntegerBased(threadCount, incsPerThread).count();
    }

}
