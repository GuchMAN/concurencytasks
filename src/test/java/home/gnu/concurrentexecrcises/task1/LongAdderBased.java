package home.gnu.concurrentexecrcises.task1;

import java.util.concurrent.atomic.LongAdder;

public class LongAdderBased extends AtomicCounterAbstract {
    private final LongAdder longAdder;

    public LongAdderBased(int threadCount, int incsPerThread) {
        super(threadCount, incsPerThread);
        longAdder = new LongAdder();
    }

    @Override
    public int getResult() {
        return longAdder.intValue();
    }

    @Override
    public void inc() {
        longAdder.increment();
    }
}
