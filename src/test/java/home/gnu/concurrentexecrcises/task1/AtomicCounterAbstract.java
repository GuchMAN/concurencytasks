package home.gnu.concurrentexecrcises.task1;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;

/**
 * Есть счетчик, подсчитывающий количество вызовов.
 * <p>
 * Почему счетчик показывает разные значения и не считает до конца?
 * Как это можно исправить не используя synchronized?
 * <p>
 * Попробуйте закомментировать обращение к yield().
 * Измениться ли значение?
 */
public abstract class AtomicCounterAbstract {

    private final int threadCount;
    private final int incsPerThread;

    public AtomicCounterAbstract(int threadCount, int incsPerThread) {

        this.threadCount = threadCount;
        this.incsPerThread = incsPerThread;
    }

    private class TestThread implements Runnable {
        private final String threadName;

        TestThread(int threadNumber) {
            this.threadName = "Thread#" + threadNumber;
        }

        @Override
        public void run() {
            for (int i = 0; i < incsPerThread; i++) {
                inc();
                Thread.yield();
            }
        }
    }

    public int count() {
        List<Thread> threads = range(0, threadCount)
                .mapToObj(TestThread::new)
                .map(Thread::new)
                .collect(Collectors.toList());

        threads.forEach(Thread::start);

        threads.forEach(this::silentlyJoin);
        return getResult();
    }

    private void silentlyJoin(Thread thread) {
        try {
            thread.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    public abstract int getResult();

    public abstract void inc();
}